#include "base64.h"
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>
#include <string.h>

char* base64_encode(const char* string)
{
    BIO *bio, *b64;
    BUF_MEM *buffer;
    char* buf;
    b64 = BIO_new(BIO_f_base64());
	bio = BIO_new(BIO_s_mem());
	bio = BIO_push(b64, bio);
	BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL);
	BIO_write(bio, string, strlen(string));
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &buffer);
    BIO_set_close(bio, BIO_NOCLOSE);
    BIO_free_all(bio);
    //return buffer->data;
    buf=malloc(buffer->length+2);
    memcpy(buf, buffer->data, buffer->length);
    buf[buffer->length]=0;
    strcat(buf,"\r\n");
    BUF_MEM_free(buffer);
    return buf;
}
