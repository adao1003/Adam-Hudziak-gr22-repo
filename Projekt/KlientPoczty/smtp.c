#include <gtk/gtk.h>
#include "settings.h"
#include "connect.h"
#include "smtp.h"
#include "interface.h"

enum
{
    SERVER_CONNECTION_ERROR = -1,
    STMP_CONECTION_ERROR = -2,
    LOGIN_ERROR = -3,
    UNCORRECT_RECEIVER = -4,
    SEND_SUCCESS =0
};
int smtp_send(const gchar* to, const gchar* topic, const gchar* message)
{
    struct connection* conn = connect_ssl(setting_data.SMTPServer);
    if(conn==NULL)
        return SERVER_CONNECTION_ERROR;
    char buf[250] = {0};
    char* username = setting_data.login;
    char* usernameb64 = base64_encode(username);
    char* passwordb64 = base64_encode(setting_data.password);
    int dataRead;
    int response =-1;
    dataRead=BIO_read(conn->bio, buf, 249);
    sscanf(buf, "%d", &response);
    if(response!=220)
    {
        printf("%s", buf);
        disconnect_ssl(conn);
        free(usernameb64);
        free(passwordb64);
        return STMP_CONECTION_ERROR;
    }
    //EHLO
    sprintf(buf,"EHLO %s\r\n", "Klient");
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 249);
    //AUTH LOGIN
    BIO_write(conn->bio, "AUTH LOGIN\r\n", 12);
    dataRead=BIO_read(conn->bio, buf, 249);
    //LOGIN
    BIO_write(conn->bio, usernameb64, strlen(usernameb64));
    dataRead=BIO_read(conn->bio, buf, 249);
    BIO_write(conn->bio, passwordb64, strlen(passwordb64));
    dataRead=BIO_read(conn->bio, buf, 249);
    sscanf(buf, "%d", &response);
    if(response!=235)
    {
        printf("%s", buf);
        disconnect_ssl(conn);
        free(usernameb64);
        free(passwordb64);
        return LOGIN_ERROR;
    }
    sprintf(buf, "mail from:<%s>\r\n", username);
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 249);
    sprintf(buf, "rcpt to:<%s>\r\n", to);
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 249);
    sscanf(buf, "%d", &response);
    if(response!=250)
    {
        printf("%s", buf);
        disconnect_ssl(conn);
        free(usernameb64);
        free(passwordb64);
        return UNCORRECT_RECEIVER;
    }
    //data
    BIO_write(conn->bio, "data\r\n", 6);
    dataRead=BIO_read(conn->bio, buf, 249);
    sprintf(buf, "From: %s\r\n", username);
    BIO_write(conn->bio, buf, strlen(buf));
    sprintf(buf, "To: %s\r\n", to);
    BIO_write(conn->bio, buf, strlen(buf));
    sprintf(buf, "Subject: %s\r\n", topic);
    BIO_write(conn->bio, buf, strlen(buf));
    BIO_write(conn->bio, message, strlen(message));
    BIO_write(conn->bio, "\r\n.\r\n", 5);
    dataRead=BIO_read(conn->bio, buf, 249);
    sscanf(buf, "%d", &response);
    BIO_write(conn->bio, "quit\r\n", 6);
    if(response==250)
    {
        disconnect_ssl(conn);
        free(usernameb64);
        free(passwordb64);
        return SEND_SUCCESS;
    }
    disconnect_ssl(conn);
    free(usernameb64);
    free(passwordb64);
    return 666;
}
gchar* lf_to_crlf(gchar* string)
{
    int len = strlen(string);
    int lineCount = 0;
    for (int i = 0; i<len;i++)
        if(string[i]=='\n') lineCount++;
    gchar* buf = malloc(len+lineCount);
    buf[0]=0;
    gchar* temp = strtok(string, "\n");
    while(temp!=NULL)
    {
        strcat(buf, temp);
        strcat(buf, "\r\n");
        temp = strtok(NULL, "\n");
    }
    g_free(string);
    return buf;
}
void send_mail(GtkButton *button, gpointer sMail)
{
    GtkEntryBuffer* toBuf = gtk_entry_get_buffer(((struct sendmail*)sMail)->to);
    GtkEntryBuffer* topicBuf = gtk_entry_get_buffer(((struct sendmail*)sMail)->topic);
    GtkTextBuffer* messageBuf = gtk_text_view_get_buffer(((struct sendmail*)sMail)->message);
    GtkTextIter begin;
    GtkTextIter end;
    gtk_text_buffer_get_start_iter(messageBuf, &begin);
    gtk_text_buffer_get_end_iter(messageBuf, &end);
    gchar* messageText = gtk_text_buffer_get_text(messageBuf, &begin, &end, FALSE);
    messageText=lf_to_crlf(messageText);
    gchar* toText = gtk_entry_buffer_get_text(toBuf);
    gchar* topicText = gtk_entry_buffer_get_text(topicBuf);
    int result = smtp_send(toText, topicText, messageText);
    switch(result)
    {
    case SERVER_CONNECTION_ERROR:
        create_dialog(((struct sendmail*)sMail)->window, "Błedny adres serwera", GTK_MESSAGE_ERROR);
        break;
    case STMP_CONECTION_ERROR:
        create_dialog(((struct sendmail*)sMail)->window, "Błąd połaczenia z serwerem", GTK_MESSAGE_ERROR);
        break;
    case LOGIN_ERROR:
        create_dialog(((struct sendmail*)sMail)->window, "Błędne dane logowania", GTK_MESSAGE_ERROR);
        break;
    case UNCORRECT_RECEIVER:
        create_dialog(((struct sendmail*)sMail)->window, "Błędny odbiorca", GTK_MESSAGE_ERROR);
        break;
    case SEND_SUCCESS:
        create_dialog(((struct sendmail*)sMail)->window, "Wiadomość wysłana", GTK_MESSAGE_INFO);
        gtk_widget_destroy(((struct sendmail*)sMail)->window);
        break;
    default:
        create_dialog(((struct sendmail*)sMail)->window, "Nieznany błąd", GTK_MESSAGE_ERROR);
        break;
    }
    g_free(messageText);
}
