#ifndef POP3_H_INCLUDED
#define POP3_H_INCLUDED
/* Wydziela nag�owki (Date, Subject, From) oraz zapisuje je GtkListStore
Zwraca: TRUE - powodzenie (odnalaz� nag�owki), FALSE - niepowodzenie*/
void decode_header(char* data, GtkWidget* tree, int id);
/* Pobiera wiadomo�ci i zapisuje do pliku. */
int pop3_receive(GtkMenuItem* menuitem, gpointer tree);

#endif // POP3_H_INCLUDED
