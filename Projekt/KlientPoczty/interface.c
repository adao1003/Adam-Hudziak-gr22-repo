#include <gtk/gtk.h>
#include "smtp.h"
#include "settings.h"
#include "mail_storage.h"
#include "pop3.h"
void create_dialog (GtkWidget* window, const gchar* message, GtkDialogFlags type)
{
    GtkWidget* dialog = gtk_message_dialog_new(window, GTK_DIALOG_MODAL, type, GTK_BUTTONS_OK, message, NULL);
    gtk_dialog_run(dialog);
    gtk_widget_destroy(dialog);
};
void new_mail_destroyed(GtkWidget *object, gpointer sMail)
{
    free (sMail);
}
void new_mail(GtkMenuItem* menuitem, gpointer user_data)
{
    GtkWidget* mailWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(mailWindow), "Mail");
    gtk_window_set_default_size(GTK_WINDOW(mailWindow), 600, 400);
    GtkWidget* box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 1);
    //send
    GtkWidget* bbox = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
    GtkWidget* send = gtk_button_new_with_label("Wyślij");
    //to
    GtkWidget* to = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
    GtkWidget* toLabel = gtk_label_new("Do:");
    gtk_label_set_width_chars(toLabel, 10);
    gtk_label_set_justify(toLabel, GTK_JUSTIFY_LEFT);
    GtkWidget* toEntry = gtk_entry_new();
    //topic
    GtkWidget* topic = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 1);
    GtkWidget* topicLabel = gtk_label_new("Temat:");
    gtk_label_set_width_chars(topicLabel, 10);
    gtk_label_set_justify(topicLabel, GTK_JUSTIFY_LEFT);
    GtkWidget* topicEntry = gtk_entry_new();
    GtkWidget* message = gtk_text_view_new();
    GtkWidget* messageBox = gtk_scrolled_window_new(NULL, NULL);
    struct sendmail* sMail = malloc(sizeof(struct sendmail));
    sMail->window=mailWindow;
    sMail->to=toEntry;
    sMail->topic=topicEntry;
    sMail->message=message;
    g_signal_connect(G_OBJECT(mailWindow), "destroy", G_CALLBACK(new_mail_destroyed), sMail);
    //Button box
    gtk_button_box_set_layout(bbox, GTK_BUTTONBOX_START);
    gtk_box_pack_start(bbox, send, FALSE, FALSE, 10);
    gtk_box_pack_start(box, bbox, FALSE, FALSE, 1);
    g_signal_connect(G_OBJECT(send), "clicked", G_CALLBACK(send_mail), sMail);
    //To
    gtk_box_pack_start(to, toLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(to, toEntry, TRUE, TRUE, 0);
    gtk_box_pack_start(box, to, FALSE, FALSE, 1);
    //Topic
    gtk_box_pack_start(topic, topicLabel, FALSE, FALSE, 0);
    gtk_box_pack_start(topic, topicEntry, TRUE, TRUE, 0);
    gtk_box_pack_start(box, topic, FALSE, FALSE, 1);
    //Massage Box
    gtk_container_add(GTK_CONTAINER(messageBox), message);
    gtk_box_pack_start(box, messageBox, TRUE, TRUE, 5);
    gtk_container_add(GTK_CONTAINER(mailWindow), box);
    gtk_widget_show_all(mailWindow);
}
void rodo_toggled (GtkToggleButton *togglebutton, gpointer rodoBool)
{
    if(*((gboolean*)rodoBool)==FALSE)
        *((gboolean*)rodoBool)=TRUE;
    else
        *((gboolean*)rodoBool)=FALSE;
}
void settings_menu(GtkMenuItem* menuitem, gpointer parent)
{
    GtkWidget* settingWindow = gtk_dialog_new_with_buttons("Ustawienia", parent, GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,"_OK", GTK_RESPONSE_ACCEPT,"_Anuluj",  GTK_RESPONSE_REJECT, NULL);
    GtkWidget* content = gtk_dialog_get_content_area(settingWindow);
    //user
    GtkWidget* userBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget* userLabel = gtk_label_new("Login:");
    GtkWidget* userEntry = gtk_entry_new();
    gtk_entry_set_text(userEntry, setting_data.login);
    gtk_box_pack_start (userBox, userLabel, FALSE, FALSE, 0);
    gtk_box_pack_start (userBox, userEntry, TRUE, TRUE, 0);
    gtk_box_pack_start (content, userBox, TRUE, TRUE, 0);
    //password
    GtkWidget* passBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget* passLabel = gtk_label_new("Hasło:");
    GtkWidget* passEntry = gtk_entry_new();
    gtk_entry_set_visibility(passEntry, FALSE);
    gtk_box_pack_start (passBox, passLabel, FALSE, FALSE, 0);
    gtk_box_pack_start (passBox, passEntry, TRUE, TRUE, 0);
    gtk_box_pack_start (content, passBox, TRUE, TRUE, 0);
    //stmp
    GtkWidget* smtpBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget* smtpLabel = gtk_label_new("SMTP:");
    GtkWidget* smtpEntry = gtk_entry_new();
    gtk_entry_set_text(smtpEntry, setting_data.SMTPServer);
    gtk_box_pack_start (smtpBox, smtpLabel, FALSE, FALSE, 0);
    gtk_box_pack_start (smtpBox, smtpEntry, TRUE, TRUE, 0);
    gtk_box_pack_start (content, smtpBox, TRUE, TRUE, 0);
    //pop3
    GtkWidget* pop3Box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget* pop3Label = gtk_label_new("POP3:");
    GtkWidget* pop3Entry = gtk_entry_new();
    gtk_entry_set_text(pop3Entry, setting_data.POP3Server);
    gtk_box_pack_start (pop3Box, pop3Label, FALSE, FALSE, 0);
    gtk_box_pack_start (pop3Box, pop3Entry, TRUE, TRUE, 0);
    gtk_box_pack_start (content, pop3Box, TRUE, TRUE, 0);
    //RODO
    gboolean rodoBool = FALSE;
    GtkWidget* rodo = gtk_check_button_new_with_label("Wyrażam zgode na przetwarzanie moich danych osobowych");
    g_signal_connect(G_OBJECT(rodo), "toggled", G_CALLBACK(rodo_toggled), &rodoBool);
    gtk_box_pack_start (content, rodo, TRUE, TRUE, 0);
    //mailDownload
    GtkWidget* mailDownloadBox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    GtkWidget* mailDownloadLabel = gtk_label_new("Ilość mail do pobrania:");
    GtkAdjustment* mailDownloadAdjustment = gtk_adjustment_new(10, 0, 1000000, 1, 5,0);
    GtkWidget* mailDownloadSpin = gtk_spin_button_new(mailDownloadAdjustment, 1, 0);
    gtk_spin_button_set_value(mailDownloadSpin, setting_data.mailDownload);
    gtk_box_pack_start (mailDownloadBox, mailDownloadLabel, FALSE, FALSE, 0);
    gtk_box_pack_start (mailDownloadBox, mailDownloadSpin, TRUE, TRUE, 0);
    gtk_box_pack_start (content, mailDownloadBox, TRUE, TRUE, 0);

    gtk_widget_show_all (settingWindow);
    while(gtk_dialog_run(GTK_DIALOG(settingWindow))==GTK_RESPONSE_ACCEPT)
    {
        gchar* userText = gtk_entry_get_text(userEntry);
        gchar* passText = gtk_entry_get_text(passEntry);
        gchar* smtpText = gtk_entry_get_text(smtpEntry);
        gchar* pop3Text = gtk_entry_get_text(pop3Entry);
        strcpy(setting_data.login, userText);
        strcpy(setting_data.password, passText);
        strcpy(setting_data.SMTPServer, smtpText);
        strcpy(setting_data.POP3Server, pop3Text);
        setting_data.mailDownload=gtk_spin_button_get_value_as_int(mailDownloadSpin);
        if(rodoBool==FALSE)
        {
            create_dialog(NULL, "RODO: \nBrak zgody na przetwarzanie danych osobowych!", GTK_MESSAGE_ERROR);
            continue;
        }
        break;
    }
    settings_save();
    gtk_widget_destroy(settingWindow);
}
void create_menu(GtkWidget* window, GtkWidget* box, GtkWidget* tree)
{
    GtkWidget* menubar = gtk_menu_bar_new();
    GtkWidget* filemenu = gtk_menu_new();
    GtkWidget* optionsmenu = gtk_menu_new();

    GtkWidget* file = gtk_menu_item_new_with_mnemonic("Plik");
    GtkWidget* new = gtk_menu_item_new_with_mnemonic("Nowy mail");
    GtkWidget* download = gtk_menu_item_new_with_mnemonic("Pobierz mail");
    GtkWidget* exit = gtk_menu_item_new_with_mnemonic("Wyjdz");

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(file), filemenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), new);
    gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), download);
    gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), gtk_separator_menu_item_new());
    gtk_menu_shell_append(GTK_MENU_SHELL(filemenu), exit);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), file);
    g_signal_connect(G_OBJECT(new), "activate", G_CALLBACK(new_mail), window);
    g_signal_connect(G_OBJECT(download), "activate", G_CALLBACK(pop3_receive), tree);
    g_signal_connect_swapped(G_OBJECT(exit), "activate", G_CALLBACK(gtk_widget_destroy), window);

    GtkWidget* opcions = gtk_menu_item_new_with_mnemonic("Opcje");
    GtkWidget* settings = gtk_menu_item_new_with_mnemonic("Ustawienia");

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(opcions), optionsmenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(optionsmenu), settings);
    gtk_menu_shell_append(GTK_MENU_SHELL(menubar), opcions);
    g_signal_connect(G_OBJECT(settings), "activate", G_CALLBACK(settings_menu), window);
    gtk_box_pack_start(box, menubar, FALSE, FALSE, 0);
}
void row_activated(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data)
{
    GtkWidget* dialog = gtk_dialog_new();
    gtk_window_set_default_size(GTK_WINDOW(dialog), 600, 400);
    GtkWidget* content = gtk_dialog_get_content_area(dialog);
    GtkTreeModel * model;
    GtkTreeIter iter;
    model = gtk_tree_view_get_model(tree_view);
    if(gtk_tree_model_get_iter(model, &iter, path))
    {
        char* from;
        char* date;
        char* subject;
        char filepath[250];
        int id;
        gtk_tree_model_get(model, &iter, MAIL_SUBJECT, &subject, MAIL_FROM, &from, MAIL_DATE, &date, MAIL_ID, &id, -1);
        GtkWidget* dateLabel = gtk_label_new(date);
        GtkWidget* fromLabel = gtk_label_new(from);
        GtkWidget* subjectLabel = gtk_label_new(subject);
        GtkWidget* message = gtk_text_view_new();
        GtkWidget* messageBox = gtk_scrolled_window_new(NULL, NULL);
        gtk_text_view_set_editable (message, FALSE);
        GtkTextBuffer* buf = gtk_text_view_get_buffer(message);
        sprintf(filepath, ".\\mails\\%d.txt", id);
        FILE* temp = fopen(filepath, "rb");
        fseek(temp, 0, SEEK_END);
        int size = ftell(temp);
        fseek(temp, 0, SEEK_SET);
        char* buffer = malloc(size+1);
        fread(buffer, size, 1, temp);
        buffer[size]=0;
        fclose(temp);
        gtk_text_buffer_set_text(buf, buffer, size);
        free(buffer);
        free(from);
        free(date);
        free(subject);
        gtk_container_add(GTK_CONTAINER(messageBox), message);
        gtk_box_pack_start(content, dateLabel, FALSE, FALSE, 5);
        gtk_box_pack_start(content, fromLabel, FALSE, FALSE, 5);
        gtk_box_pack_start(content, subjectLabel, FALSE, FALSE, 5);
        gtk_box_pack_start(content, messageBox, TRUE, TRUE, 5);
        gtk_widget_show_all(dialog);
        gtk_dialog_run(dialog);
        gtk_widget_destroy(dialog);
    }
}
GtkWidget* mail_list(GtkWidget* window, GtkWindow* box)
{
    GtkTreeIter iter;
    GtkListStore* mailStorage = gtk_list_store_new(MAIL_STORAGE_COLUMNS_NR, G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
    GtkWidget* tree = gtk_tree_view_new_with_model(mailStorage);
    g_object_unref (G_OBJECT (mailStorage));
    GtkTreeViewColumn* date = gtk_tree_view_column_new();
    GtkCellRenderer* dateRenderer = gtk_cell_renderer_text_new();
    gtk_tree_view_column_set_title(date, "Data");
    gtk_tree_view_column_pack_start(date, dateRenderer, TRUE);
    gtk_tree_view_column_add_attribute( date, GTK_CELL_RENDERER(dateRenderer), "text", MAIL_DATE );
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), date );
    //
    GtkTreeViewColumn* from = gtk_tree_view_column_new();
    GtkCellRenderer* fromRenderer = gtk_cell_renderer_text_new();
    gtk_tree_view_column_set_title(from, "Od");
    gtk_tree_view_column_pack_start(from, fromRenderer, TRUE);
    gtk_tree_view_column_add_attribute( from, GTK_CELL_RENDERER(fromRenderer), "text", MAIL_FROM );
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), from );
    //
    GtkTreeViewColumn* subject = gtk_tree_view_column_new();
    GtkCellRenderer* subjectRenderer = gtk_cell_renderer_text_new();
    gtk_tree_view_column_set_title(subject, "Temat");
    gtk_tree_view_column_pack_start(subject, subjectRenderer, TRUE);
    gtk_tree_view_column_add_attribute( subject, GTK_CELL_RENDERER(subjectRenderer), "text", MAIL_SUBJECT );
    gtk_tree_view_append_column(GTK_TREE_VIEW(tree), subject );
    g_signal_connect(tree, "row-activated",(GCallback)row_activated, NULL );
    return tree;
}
void init(GtkApplication* app, gpointer user_data)
{
    GtkWidget* window;
    GtkWindow* box;
    window = gtk_application_window_new(app);
    box=gtk_box_new(GTK_ORIENTATION_VERTICAL, 1);
    gtk_container_add(GTK_CONTAINER(window), box);
    gtk_window_set_title(GTK_WINDOW(window), "Klient");
    gtk_window_set_default_size(GTK_WINDOW(window), 600, 400);
    GtkWidget* tree = mail_list(window, box);
    GtkWidget* treeBox = gtk_scrolled_window_new(NULL, NULL);
    create_menu(window, box, tree);
    gtk_container_add(GTK_CONTAINER(treeBox), tree);
    gtk_box_pack_start(box, treeBox, TRUE, TRUE, 10);
    storage_read_from_file(tree);
    gtk_widget_show_all(window);
}
