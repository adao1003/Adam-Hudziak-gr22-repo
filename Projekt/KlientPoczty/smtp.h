#ifndef SMTP_H_INCLUDED
#define SMTP_H_INCLUDED
struct sendmail
{
    GtkWidget* window;
    GtkWidget* to;
    GtkWidget* topic;
    GtkWidget* message;
};
/* wysy�a maila przez SMTP.
Zwraca: 0 - powodzenie, kod b�edu - w przypadku niepowodzenia */
int smtp_send(const gchar* to, const gchar* topic, const gchar* message);
/*Zamienia znak ko�ca linii LF to CRLF w celu wysta�nia wiadomo�ci przez SMTP.
Funkcja niszczy oryginalny string oraz go zwalnia z pami�ci*/
gchar* lf_to_crlf(gchar* string);
/* Funkcja obs�uguje sygna� z przycisku "Wy�lij".
Przygotowuje dane do wys�ania prze SMTP oraz wy�wietla komunikaty o b�edach */
void send_mail(GtkButton *button, gpointer sMail);


#endif // SMTP_H_INCLUDED
