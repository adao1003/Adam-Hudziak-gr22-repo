#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <openssl/crypto.h>
#include "interface.h"
#include "settings.h"

int main(int argc, char** argv)
{
    GtkApplication* app;
    app = gtk_application_new(NULL, G_APPLICATION_FLAGS_NONE);
    g_signal_connect(app, "activate", G_CALLBACK(init), NULL);
    SSL_load_error_strings();
    ERR_load_BIO_strings();
    OpenSSL_add_all_algorithms();
    settings_load();
    //pop3_receive();
    int status = g_application_run(G_APPLICATION(app), argc, argv);
    g_object_unref (app);
    settings_save();
    return status;
}
