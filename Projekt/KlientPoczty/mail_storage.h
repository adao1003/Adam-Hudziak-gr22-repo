#ifndef MAIL_STORAGE_H_INCLUDED
#define MAIL_STORAGE_H_INCLUDED
#include<gtk/gtk.h>
/* Infomacje o polach w GtkListStore */
enum
{
    MAIL_ID = 0,
    MAIL_SUBJECT,
    MAIL_FROM,
    MAIL_DATE,
    MAIL_STORAGE_COLUMNS_NR
};
/* Odczytuje GtkListStorage z pliku*/
void storage_read_from_file(GtkWidget* tree);
/* Dodaje element do GtkListStore kt�re przeochuje informacje o mailach*/
void add_to_storage(GtkListStore* store, int id, const char* subject, const char* from, const char* date);
gboolean storage_save_to_file_foreach(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data);
/* Zapisuje GtkListStorage to pliku*/
void storage_save_to_file(GtkWidget* tree);
#endif // MAIL_STORAGE_H_INCLUDED
