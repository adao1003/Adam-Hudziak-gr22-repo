#ifndef INTERFACE_H_INCLUDED
#define INTERFACE_H_INCLUDED
/* Funcja wywo�ywania po zamkieciu okna nowego maila. Zwalania zaalokowan� strukur� */
void new_mail_destroyed(GtkWidget *object, gpointer sMail);
/* Tworzy okno nowego maila. (wywo�anie sygna�u z elemetu menu) */
void new_mail(GtkMenuItem* menuitem, gpointer user_data);
/* Funkcja obs�uguje sygna� check box-a. (zmienia stan zmiennej gboolean)
rodoBool - bool*/
void rodo_toggled (GtkToggleButton *togglebutton, gpointer rodoBool);
/* Funkcja wy�wietla okno dialogowe ustawie� */
void settings_menu(GtkMenuItem* menuitem, gpointer parent);
/* Tworzy pasek menu.
window - okno g�owne
box - kontener okna glownego */
void create_menu(GtkWidget* window, GtkWidget* box, GtkWidget* tree);
/* Funkcja obs�uguje sygna� z GtkTreeView. Przechwytuje wybrany mail oraz wyswietla jego tre�� w nowym oknie dialogowym.
tree_view - wska�nik na GtkTreeView
path - �cie�ka do wybranego maila
column - wybrana kolumna
user_data - nieu�ywane (wymagane przez GTK)*/
void row_activated(GtkTreeView *tree_view, GtkTreePath *path, GtkTreeViewColumn *column, gpointer user_data);
/*Funkcja tworzy interfejs do przechowywania maili.
window - wska�nik do okna g��wnego
box - kontener okna g��wego
Funkcja zwraca wska�nik na GtkTreeView (interfejs do przechowywania maili) */
GtkWidget* mail_list(GtkWidget* window, GtkWindow* box);
/*Funkcja wykonywana na pocz�tku programu. Iniciuje dzia�anie programu.
app, user_data - wymagane przez GTK */
void init(GtkApplication* app, gpointer user_data);


#endif // INTERFACE_H_INCLUDED
