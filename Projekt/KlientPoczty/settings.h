#ifndef SETTINGS_H_INCLUDED
#define SETTINGS_H_INCLUDED
struct
{
    char login[256];
    char password[256];
    char SMTPServer[256];
    char POP3Server[256];
    int mailDownload;
}setting_data;
/* Funkcja odczytuje strukture setting_data z pliku */
void settings_load();
/* Funkcja zapisuje strukture setting_data do pliku */
void settings_save();
#endif // SETTINGS_H_INCLUDED
