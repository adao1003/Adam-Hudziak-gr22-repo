#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "settings.h"
void settings_load()
{
    FILE* data = fopen("settings.conf", "r");
    if (data==NULL)
    {
        setting_data.mailDownload=0;
    }
    char buf[256];
    char name[20];
    while(fscanf(data,"%s %*s %s \n", name, buf)!=EOF)
    {
        if(strcmp(name, "user")==0){
            sscanf(buf, "\"%s%", setting_data.login);
            setting_data.login[strlen(setting_data.login)-1]=0;}
        else if (strcmp(name, "pass")==0){
            sscanf(buf, "\"%s%", setting_data.password);
            setting_data.password[strlen(setting_data.password)-1]=0;}
        else if (strcmp(name, "smtp")==0){
            sscanf(buf, "\"%s%", setting_data.SMTPServer);
            setting_data.SMTPServer[strlen(setting_data.SMTPServer)-1]=0;}
        else if (strcmp(name, "pop3")==0){
            sscanf(buf, "\"%s%", setting_data.POP3Server);
            setting_data.POP3Server[strlen(setting_data.POP3Server)-1]=0;}
        else if (strcmp(name, "mailDownload")==0){
            sscanf(buf, "%d", &(setting_data.mailDownload));
        }
    }
    fclose(data);
}
void settings_save()
{
    FILE* data = fopen("settings.conf", "w");
    fprintf(data, "user = \"%s\"\n", setting_data.login);
    fprintf(data, "pass = \"%s\"\n", setting_data.password);
    fprintf(data, "smtp = \"%s\"\n", setting_data.SMTPServer);
    fprintf(data, "pop3 = \"%s\"\n", setting_data.POP3Server);
    fprintf(data, "mailDownload = %d \n", setting_data.mailDownload);
    fclose(data);
}
