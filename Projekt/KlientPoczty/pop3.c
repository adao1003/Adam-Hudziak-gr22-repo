#include <gtk/gtk.h>
#include "settings.h"
#include "connect.h"
#include "mail_storage.h"
#include <string.h>
gboolean decode_header(char* data, GtkWidget* tree, int id)
{
    char* temp = strstr(data, "\r\nFrom:");
    if(temp==NULL) return FALSE;
    char* from;
    char* date;
    char* subject;
    temp = strtok(temp, "\r");
    temp+=7;
    from = strdup(temp);
    temp[strlen(temp)]='\r';
    temp = strstr(data, "\r\nDate:");
    temp = strtok(temp, "\r");
    temp+=7;
    date = strdup(temp);
    temp[strlen(temp)]='\r';
    temp = strstr(data, "\r\nSubject:");
    temp = strtok(temp, "\r");
    temp+=10;
    subject = strdup(temp);
    temp[strlen(temp)]='\r';
    //printf("%d\t%s\t%s\t%s\n", id, subject, from, date);
    GtkTreeModel* store = gtk_tree_view_get_model(tree);
    //printf("%p", store);
    //system("pause");
    add_to_storage(store, id, subject, from, date);
    free(from);
    free(date);
    free(subject);
    return TRUE;
   //printf("%s\n", temp);
    //system("pause");
    //printf("%s", temp);
}
int pop3_receive(GtkMenuItem* menuitem, gpointer tree)
{
    struct connection* conn = connect_ssl(setting_data.POP3Server);
    if(conn==NULL){
        create_dialog(NULL, "Bład połacznie z serwerem!", GTK_MESSAGE_ERROR);
        return -1;
    }
    char buf[10000] = {0};
    char response [20] = {0};
    int dataRead;
    int stat = 0;
    dataRead=BIO_read(conn->bio, buf, 9999);
    buf[dataRead]=0;
    printf("%s", buf);
    sprintf(buf, "user %s\r\n", setting_data.login);
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 9999);
    sscanf(buf, "%s", response);
    if((strcmp(response,"+OK"))!=0)
    {
        create_dialog(NULL, "Błedne dane logowania!", GTK_MESSAGE_ERROR);
        disconnect_ssl(conn);
        return -1;
    }
    sprintf(buf, "pass %s\r\n", setting_data.password);
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 9999);
    sscanf(buf, "%s", response);
    if((strcmp(response,"+OK"))!=0)
    {
        create_dialog(NULL, "Błedne dane logowania!", GTK_MESSAGE_ERROR);
        disconnect_ssl(conn);
        return -1;
    }
    sprintf(buf, "stat\r\n");
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 9999);
    buf[dataRead]=0;
    sscanf(buf, "%*s %d %*d", &stat);
    printf("%d", stat);
    //system("pause");
    GtkTreeModel* store = gtk_tree_view_get_model(tree);
    gtk_list_store_clear (store);
    for(int i=0;i<setting_data.mailDownload;i++)
    {
        if(stat-i<0)
            break;
        sprintf(buf, "retr %d\r\n", stat-i);
        BIO_write(conn->bio, buf, strlen(buf));
        char filename[30];
        sprintf(filename, ".\\mails\\%d.txt", i);
        //printf("%s", filename);
        FILE* out = fopen(filename, "wb");
        //printf("%d:%s",dataRead,  buf);
        //system("pause");
        do
        {
            dataRead=BIO_read(conn->bio, buf, 9999);
            buf[dataRead]=0;
            fwrite(buf, dataRead, 1, out);
        }while(!decode_header(buf, tree, i));
        while((strstr(buf, "\r\n.\r\n"))==NULL)
        {
            dataRead=BIO_read(conn->bio, buf, 9999);
            fwrite(buf, dataRead, 1, out);
            buf[dataRead]=0;
            //printf("%d\n", dataRead);
        }
        fclose(out);
    }
    sprintf(buf, "quit\r\n");
    BIO_write(conn->bio, buf, strlen(buf));
    dataRead=BIO_read(conn->bio, buf, 9999);
    storage_save_to_file(tree);
    disconnect_ssl(conn);
}
