#include<gtk/gtk.h>
#include "mail_storage.h"
void add_to_storage(GtkListStore* store, int id, const char* subject, const char* from, const char* date)
{
    GtkTreeIter iter;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter, MAIL_SUBJECT, subject, MAIL_ID, id, MAIL_FROM, from, MAIL_DATE, date, -1);
}
void storage_read_from_file(GtkWidget* tree)
{
    FILE* in = fopen(".\\mails\\database.db", "r");
    if(in==NULL)
        return;
    int id;
    char from[1000];
    char date[1000];
    char subject[1000];
    GtkListStore* storage = gtk_tree_view_get_model(tree);
    while(fscanf(in, "%d\n", &id)!=EOF){
    fgets(from, 1000, in);
    from[strlen(from)-1]=0;
    fgets(date, 1000, in);
    date[strlen(date)-1]=0;
    fgets(subject, 1000, in);
    subject[strlen(subject)-1]=0;
    add_to_storage(storage, id, subject, from, date);
    }
    fclose(in);
}
gboolean storage_save_to_file_foreach (GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
    int id;
    char* from;
    char* date;
    char* subject;
    gtk_tree_model_get(model, iter, MAIL_SUBJECT, &subject, MAIL_FROM, &from, MAIL_DATE, &date, MAIL_ID, &id, -1);
    fprintf(data, "%d\n%s\n%s\n%s\n", id, from, date, subject);
    free(from);
    free(date);
    free(subject);
    return FALSE;
}
void storage_save_to_file(GtkWidget* tree)
{
    GtkListStore* storage = gtk_tree_view_get_model(tree);
    FILE* out = fopen(".\\mails\\database.db", "w");
    gtk_tree_model_foreach(storage, storage_save_to_file_foreach, out);
    fclose(out);
}
