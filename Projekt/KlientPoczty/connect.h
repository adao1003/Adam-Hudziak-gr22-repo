#ifndef CONNECT_H_INCLUDED
#define CONNECT_H_INCLUDED
#include "openssl/bio.h"
struct connection
{
    BIO* bio;
    SSL_CTX * ctx;
    SSL * ssl;
};
/* Funkcja ��czy si� z serwerem o podanym adresie wraz z portem.
Zwraca struktur� przechowuj�c� informacje o po��czeniu*/
struct connection* connect_ssl(const char* hostname);
/* Ko�czy po��czenie z serwerem i zwalnia struktur�*/
void disconnect_ssl(struct connection* con);
#endif // CONNECT_H_INCLUDED
