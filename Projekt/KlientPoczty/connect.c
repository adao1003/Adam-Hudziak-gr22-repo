#include "openssl/bio.h"
#include "openssl/ssl.h"
#include "openssl/err.h"
#include "connect.h"
struct connection* connect_ssl(const char* hostname)
{
    struct connection* con = malloc(sizeof(struct connection));
    con->ctx = SSL_CTX_new(SSLv23_client_method());
    con->bio = BIO_new_ssl_connect(con->ctx);
    if(!(SSL_CTX_load_verify_locations(con->ctx, NULL, ".\certs")))
    {
        free(con);
        printf("zUo");
        return NULL;
    }
    //BIO_set_close(con->bio, BIO_NOCLOSE);
    BIO_get_ssl(con->bio, &(con->ssl));
    SSL_set_mode(con->ssl, SSL_MODE_AUTO_RETRY);
    BIO_set_conn_hostname(con->bio, hostname);
    if(BIO_do_connect(con->bio) <= 0)
    {
        BIO_free_all(con->bio);
        SSL_CTX_free(con->ctx);
        free(con);
        return NULL;
    }
    return con;
}
void disconnect_ssl(struct connection* con)
{
    BIO_free_all(con->bio);
    SSL_CTX_free(con->ctx);
    free(con);
    return;
};
