#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char* read(FILE* in, int* size)
{
	fseek(in, 0, SEEK_END);
	*size = ftell(in);
	char* buf = malloc(*size + 1);
	fseek(in, 0, SEEK_SET);
	fread(buf, sizeof(char), *size, in);
	buf[*size] = '\0';
	return buf;
}
void copy(FILE* out, char* buffer, int size)
{
	fwrite(buffer, sizeof(char), size, out);
}
void replace(FILE* out, char* buffer, int size)
{
	for (int i = 0;i < size;i++)
	{
		if (islower(buffer[i]))
			buffer[i] = toupper(buffer[i]);
		else if (isupper(buffer[i]))
			buffer[i] = tolower(buffer[i]);
	}
	fwrite(buffer, sizeof(char), size, out);
}
void palindrom(FILE* out, char* buffer, int size)
{
	char* temp, *temp2;
	temp = strtok(buffer, " ");
	for (;;)
	{
		//temp2 = malloc(strlen(temp));
		temp2 = _strdup(temp);
		temp2 = strrev(temp2);
		if (strcmp(temp, temp2) == 0)
			fprintf(out, "%s \n", temp);
		temp = strtok(NULL, " ");
		if (temp == NULL)
			return;
		free(temp2);
	}
}

int main(int argc, char** argv)
{
	FILE* in;
	FILE* out;
	char* buffer;
	char* mode[] = { "-copy","-replace","-palindrom" };
	int m = -1;
	int size;
	if (argc != 4)
	{
		printf("Bledna liczba argumentów \n");
		system("pause");

		return -10;
	}
	for (int i = 0;i < 3;i++)
	{
		if (strcmp(mode[i], argv[3])==0)
		{
			m = i;
			break;
		}
	}
	if (m == -1)
	{
		printf("Bledny tryb \n");
		system("pause");
		return -1;
	}
	if ((in = fopen(argv[1], "rb")) == NULL)
	{
		printf("Blad wejscia/wyjscia \n");
		system("pause");
		return -2;
	}
	if ((out = fopen(argv[2], "wb")) == NULL)
	{
		printf("Blad wejscia/wyjscia \n");
		system("pause");
		return -2;
	}
	buffer=read(in, &size);
	switch(m)
	{
	case 0:
		copy(out, buffer, size);
		break;
	case 1:
		replace(out, buffer, size);
		break;
	case 2:
		palindrom(out, buffer, size);
		break;
	}
	free(buffer);
	fclose(in);
	fclose(out);
}