#include <stdio.h>
#include <stdlib.h>
#include <conio.h>

enum state {wybor, odtwarzanie, koniec};
enum event {rozpocznij, zatrzymaj, zakoncz};
char* wybierz(char* nic)
{
	int temp;
	static const char* gat[5] = { "pop", "rock", "blues", "disco", "jazz" };
	printf("Wybierz gatunek \n");
	for (int i = 0;i < 5;i++)
	{
		printf("%d. %s \n", i, gat[i]);
	}
	scanf("%d", &temp);
	return gat[temp];
}
char* odtworz(char* gatunek)
{
	printf("Aktualnie odwarzane: %s \n", gatunek);
	return gatunek;
}
void rozpoczenie()
{
	printf("Rozpoczęto odtwarzanie \n");
}
void zatrzynanie()
{
	printf("Zatrzymano odtwarzanie \n");
}
char* end(char* nic)
{
	exit(0);
}

char* pusta(char* ff)
{}
int main()
{
	enum state SA = wybor;
	enum state SN;
	enum state przejscie[3][3] = {{ odtwarzanie, wybor, koniec },{ odtwarzanie, wybor, koniec },{ koniec, koniec, koniec }};
	enum event zdarzenie;
	char* gatunek = NULL;
	void(*zmiany[3][3])() = { { pusta,pusta,pusta },{ pusta,pusta,pusta },{ pusta,pusta,pusta } };
	char*(*akcja[3])(char*) = { wybierz, odtworz, end };
	zmiany[wybor][odtwarzanie] = rozpoczenie;
	zmiany[odtwarzanie][wybor] = zatrzynanie;
	int key;
	gatunek = akcja[SA](gatunek);
	for (;;)
	{

		key = getch();
		switch (key)
		{
		case 'p':
			zdarzenie = rozpocznij;
			break;
		case 's':
			zdarzenie = zatrzymaj;
			break;
		case 27:
			zdarzenie = zakoncz;
			break;
		default:
			continue;
		}
		SN = przejscie[SA][zdarzenie];
		zmiany[SA][SN]();
		if (SA == SN)
			continue;
		SA = SN;
		gatunek = akcja[SA](gatunek);
	}
}