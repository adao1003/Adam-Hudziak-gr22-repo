#include <stdio.h>
#include <stdlib.h>
#define size 6

void f(float* tab, const int n)
{
	int i = 0;
	for (i = 0;i < n;i++)
	{
		scanf("%f", &tab[i]);
	}
}
void p(const float* tab, const int n)
{
	int i = 0;
	for (i = 0;i < n;i++, tab++)
	{
		printf("%f \n", *tab);
	}
}
void minmax(const float* tab, const int n, int* min, int* max)
{
	int i = 0;
	float minimum = *tab;
	float maximum = *tab;
	*min = 0;
	*max = 0;
	for (i = 0;i < n;i++, tab++)
	{
		if (*tab < minimum)
		{
			minimum = *tab;
			*min = i;
		}
		if (*tab > maximum)
		{
			maximum = *tab;
			*max = i;
		}
	}
}
int main()
{
	float tab[size];
	int max;
	int min;
	f(tab, size);
	p(tab, size);
	minmax(tab, size, &min, &max);
	printf("MIN: %f [%d] MAX: %f [%d]", tab[min], min, tab[max], max);
	system("pause");
}
