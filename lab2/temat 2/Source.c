#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct list
{
	char* name;
	struct list* next;
};
void alokuj(void** buf, int size)
{
	*buf = malloc(size);
	return;
}
void usun(void** buf)
{
	free(*buf);
	*buf = NULL;
}
void push_front(struct list** HEAD, char* name)
{
	struct list* temp = *HEAD;
	*HEAD = malloc(sizeof(struct list));
	(*HEAD)->next = temp;
	(*HEAD)->name = _strdup(name);
}
void print(struct list** HEAD)
{
	struct list* temp=(*HEAD);
	struct list* temp2 = (*HEAD);
	while (temp != NULL)
	{
		printf("%s \n", temp->name);
		temp = temp->next;
		free(temp2->name);
		free(temp2);
		temp2 = temp;
	}
	*HEAD = NULL;
}
float add(float a, float b)
{
	return a + b;
}
void transform(float* A, float* B,float* C, int size, float(*op)(float, float))
{
	int i = 0;
	for (i = 0;i < size;i++)
	{
		C[i] = op(A[i], B[i]);
	}
}
int main()
{
	struct list* HEAD = NULL;
	char* string;
	float A[10];
	float B[10];
	for (int i = 0;i < 10;i++)
	{
		A[i] = rand() % 100;
		B[i] = rand() % 100;
	}
	float C[10];
	alokuj(&string, 100);
	scanf("%s", string);
	printf("%s \n", string);
	usun(&string);
	push_front(&HEAD, "Adam");
	push_front(&HEAD, "Zbigiew");
	print(&HEAD);
	transform(A, B, C, 10, add);
	system("pause");
}